[33mcommit f3653ddb9ee0a0c17cab4d292a01f4c141481ab0[m[33m ([m[1;36mHEAD -> [m[1;32mfeature/academy-coin[m[33m, [m[1;31morigin/feature/academy-coin[m[33m)[m
Author: VolodymyrYavorskyi <83591768+VolodymyrYavorskyi@users.noreply.github.com>
Date:   Fri May 7 20:14:02 2021 +0300

    Gradle fix

[33mcommit ab376dcce9c1d5690b6dfe8796102494d85f8f87[m
Author: VolodymyrYavorskyi <83591768+VolodymyrYavorskyi@users.noreply.github.com>
Date:   Fri May 7 18:23:42 2021 +0300

    Code style fix

[33mcommit 6249c04d5cba0fd79d12f2a587b395f096a40dc4[m
Author: VolodymyrYavorskyi <83591768+VolodymyrYavorskyi@users.noreply.github.com>
Date:   Fri May 7 18:08:20 2021 +0300

    Small fix

[33mcommit 4391d13e236bcd8616b559e726564d042f67e48d[m
Author: VolodymyrYavorskyi <83591768+VolodymyrYavorskyi@users.noreply.github.com>
Date:   Fri May 7 17:48:02 2021 +0300

    AcademyCoin Task done

[33mcommit 672e77ca5f277d0ad52f872bce6855f0ccc71edd[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/develop[m[33m, [m[1;32mmaster[m[33m, [m[1;32mdevelop[m[33m)[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Tue May 4 20:12:52 2021 +0300

    Update README.UA.md
    
    Updated UA scoring typo

[33mcommit 21f68ef546eb28bc57343651181bad8950424673[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Tue May 4 20:12:25 2021 +0300

    Update README.RU.md
    
    Fixed RU scoring typo

[33mcommit 0ef43a9bef21e5ddeceabf31588cc9ccc2524ae0[m
Merge: 470b890 096e945
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Mon May 3 13:07:46 2021 +0300

    Merge pull request #2 from klesogor/2021/hw-update
    
    - added 2 new tasks

[33mcommit 096e945525fc34a53b76a32fd9f808aee43c4dbf[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Mon May 3 13:03:48 2021 +0300

    [2021] Fixed markdown in academy_coin readme

[33mcommit 8ab951f5aca51e5b59379e36bff20d0e2d45a39c[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Mon May 3 12:39:33 2021 +0300

    [2021] Fixed template to pass linter

[33mcommit 755920124e0b219eda80c311320e1b3f7406a6a0[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Fri Apr 30 20:56:56 2021 +0300

    [2021] Updated template with new tasks

[33mcommit 978f8619f24d589b6ce2d3fec5750ce88d1fce38[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Fri Apr 30 10:01:34 2021 +0300

    Updated repo description

[33mcommit 470b890023f0d0c4383f0501ac0e825e56e18b68[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Fri May 8 18:45:15 2020 +0300

    Update DockedShipBuilder.java
    
    Fixed field assignment in `speed` method. This fix will NOT affect any tests, you can ignore it

[33mcommit efa02a3e892fbd41c598572e927cf1efb8fd4400[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Thu May 7 15:05:52 2020 +0300

    Update DockedShipBuilder.java

[33mcommit efaff0a9baff2066dbe12c0b118229aec752c555[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Thu May 7 14:46:30 2020 +0300

    Updated hull and HP regeneration test

[33mcommit f83ff0c6a0681f5d4fd87f3929477e88de07863a[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Thu May 7 14:29:32 2020 +0300

    Update VesselTest.java
    
    Removed one attack

[33mcommit d48629848bf9a1b389658d30d9f69719f21773fd[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Thu May 7 14:14:13 2020 +0300

    Lint + added fields

[33mcommit ba62d469a678e26625f9bb68efd2c54df261f6ce[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Thu May 7 14:05:41 2020 +0300

    Updated oneDamageApplication test

[33mcommit 80facf90f08d46e49ebf63ca934c437a94b77512[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Thu May 7 13:44:30 2020 +0300

    Updated attacks count in description

[33mcommit 18864045f863d0cc8f1f20b2691bc8e86db0c529[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Thu May 7 13:42:23 2020 +0300

    Updated tests in types

[33mcommit 875a57c4467fbd7eebaf4d498fbbdf85b16161b1[m
Author: Kirill Lesohorskyi <lesogor.kirill@gmail.com>
Date:   Mon May 4 16:08:00 2020 +0300

    Inited template repo
