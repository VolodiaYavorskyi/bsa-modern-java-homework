package com.binary_studio.dependency_detector;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	private enum VertexState {

		NEVER_VISITED, IS_PROCESSED, VISITED

	}

	public static boolean canBuild(DependencyList libraries) {
		Map<String, List<String>> dependencyGraph = makeDependencyGraph(libraries.dependencies);
		Map<String, VertexState> vertexStates = dependencyGraph.keySet().stream()
				.collect(Collectors.toMap(key -> key, value -> VertexState.NEVER_VISITED));

		for (String vertex : dependencyGraph.keySet()) {
			if (dfs(dependencyGraph, vertexStates, vertex)) {
				return false;
			}
		}

		return true;
	}

	private static <T> Map<T, List<T>> makeDependencyGraph(List<T[]> dependencies) {
		Map<T, List<T>> res = new HashMap<>();

		for (T[] dependency : dependencies) {
			T lib = dependency[0];
			T on = dependency[1];
			res.computeIfAbsent(lib, k -> new LinkedList<>()).add(on);
		}

		return res;
	}

	private static <T> boolean dfs(Map<T, List<T>> graph, Map<T, VertexState> vertexStates, T vertex) {
		vertexStates.put(vertex, VertexState.IS_PROCESSED);

		for (T v : graph.get(vertex)) {
			if (vertexStates.get(v) == VertexState.NEVER_VISITED) {
				return dfs(graph, vertexStates, v);
			}
			else if (vertexStates.get(v) == VertexState.IS_PROCESSED) {
				return true;
			}
		}

		vertexStates.put(vertex, VertexState.VISITED);
		return false;
	}

}
