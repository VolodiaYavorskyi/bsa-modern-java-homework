package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private final PositiveInteger startShieldHp;

	private final PositiveInteger startHullHp;

	private final PositiveInteger startCapacitor;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger pg,
			PositiveInteger capacitor, PositiveInteger capacitorRegeneration, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
		this.startShieldHp = shieldHP;
		this.startHullHp = hullHP;
		this.startCapacitor = capacitor;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger pg, PositiveInteger capacitor, PositiveInteger capacitorRegeneration, PositiveInteger speed,
			PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, pg, capacitor, capacitorRegeneration, speed, size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		int powerLeft = this.pg.value();
		powerLeft -= this.defenciveSubsystem == null ? 0 : this.defenciveSubsystem.getPowerGridConsumption().value();
		powerLeft -= subsystem == null ? 0 : subsystem.getPowerGridConsumption().value();
		if (powerLeft < 0) {
			throw new InsufficientPowergridException(-powerLeft);
		}

		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		int powerLeft = this.pg.value();
		powerLeft -= this.attackSubsystem == null ? 0 : this.attackSubsystem.getPowerGridConsumption().value();
		powerLeft -= subsystem == null ? 0 : subsystem.getPowerGridConsumption().value();
		if (powerLeft < 0) {
			throw new InsufficientPowergridException(-powerLeft);
		}

		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this);
	}

	public void setShieldHP(PositiveInteger shieldHP) {
		this.shieldHP = shieldHP;
	}

	public void setHullHP(PositiveInteger hullHP) {
		this.hullHP = hullHP;
	}

	public void setCapacitor(PositiveInteger capacitor) {
		this.capacitor = capacitor;
	}

	public void setCapacitorRegeneration(PositiveInteger capacitorRegeneration) {
		this.capacitorRegeneration = capacitorRegeneration;
	}

	public void setPg(PositiveInteger pg) {
		this.pg = pg;
	}

	public void setSpeed(PositiveInteger speed) {
		this.speed = speed;
	}

	public void setSize(PositiveInteger size) {
		this.size = size;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getCapacitor() {
		return this.capacitor;
	}

	public PositiveInteger getCapacitorRegeneration() {
		return this.capacitorRegeneration;
	}

	public PositiveInteger getPg() {
		return this.pg;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public PositiveInteger getStartShieldHp() {
		return this.startShieldHp;
	}

	public PositiveInteger getStartHullHp() {
		return this.startHullHp;
	}

	public PositiveInteger getStartCapacitor() {
		return this.startCapacitor;
	}

}
