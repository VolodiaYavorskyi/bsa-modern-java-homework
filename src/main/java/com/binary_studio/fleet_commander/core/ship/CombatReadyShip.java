package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import static java.lang.Math.min;

public final class CombatReadyShip implements CombatReadyVessel {

	private final DockedShip ship;

	CombatReadyShip(DockedShip ship) {
		this.ship = ship;
	}

	@Override
	public void endTurn() {
		PositiveInteger newCapacitor = PositiveInteger.of(min(this.ship.getStartCapacitor().value(),
				this.ship.getCapacitor().value() + this.ship.getCapacitorRegeneration().value()));
		this.ship.setCapacitor(newCapacitor);
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		AttackSubsystem attackSubsystem = this.ship.getAttackSubsystem();
		AttackAction attackAction = null;

		if (this.ship.getCapacitor().value() >= attackSubsystem.getCapacitorConsumption().value()) {
			attackAction = new AttackAction(attackSubsystem.attack(target), this, target, attackSubsystem);
			this.ship.setCapacitor(PositiveInteger
					.of(this.ship.getCapacitor().value() - attackSubsystem.getCapacitorConsumption().value()));
		}

		return Optional.ofNullable(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.ship.getDefenciveSubsystem().reduceDamage(attack);
		int damageLeft = reducedAttack.damage.value();

		if (this.ship.getShieldHP().value() > damageLeft) {
			this.ship.setShieldHP(PositiveInteger.of(this.ship.getShieldHP().value() - damageLeft));
			damageLeft = 0;
		}
		else {
			damageLeft -= this.ship.getShieldHP().value();
			this.ship.setShieldHP(PositiveInteger.of(0));
		}
		if (this.ship.getHullHP().value() > damageLeft) {
			this.ship.setHullHP(PositiveInteger.of(this.ship.getHullHP().value() - damageLeft));
			damageLeft = 0;
		}

		if (damageLeft > 0) {
			return new AttackResult.Destroyed();
		}
		else {
			return new AttackResult.DamageRecived(attack.weapon, reducedAttack.damage, this);
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.ship.getHullHP().value() == 0 || this.ship.getCapacitor().value() < this.ship.getDefenciveSubsystem()
				.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		RegenerateAction regenerateAction = this.ship.getDefenciveSubsystem().regenerate();
		this.ship.setCapacitor(PositiveInteger.of(this.ship.getCapacitor().value()
				- this.ship.getDefenciveSubsystem().getCapacitorConsumption().value()));

		PositiveInteger shieldHpRegen = PositiveInteger
				.of(min(this.ship.getStartShieldHp().value() - this.ship.getShieldHP().value(),
						regenerateAction.shieldHPRegenerated.value()));
		PositiveInteger hullHpRegen = PositiveInteger
				.of(min(this.ship.getStartHullHp().value() - this.ship.getHullHP().value(),
						regenerateAction.hullHPRegenerated.value()));

		PositiveInteger newShieldHp = PositiveInteger.of(this.ship.getShieldHP().value() + shieldHpRegen.value());
		PositiveInteger newHullHp = PositiveInteger.of(this.ship.getHullHP().value() + hullHpRegen.value());

		this.ship.setShieldHP(newShieldHp);
		this.ship.setHullHP(newHullHp);

		return Optional.of(new RegenerateAction(shieldHpRegen, hullHpRegen));
	}

}
