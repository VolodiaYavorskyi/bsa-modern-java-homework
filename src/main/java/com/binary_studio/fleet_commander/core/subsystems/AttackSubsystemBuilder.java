package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public final class AttackSubsystemBuilder {

	private String name;

	private PositiveInteger pgRequirement;

	private PositiveInteger capacitorUsage;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public static AttackSubsystemBuilder named(String name) {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		var builder = new AttackSubsystemBuilder();
		builder.name = name;

		return builder;
	}

	public AttackSubsystemBuilder pg(Integer val) {
		this.pgRequirement = PositiveInteger.of(val);
		return this;
	}

	public AttackSubsystemBuilder damage(Integer val) {
		this.baseDamage = PositiveInteger.of(val);
		return this;
	}

	public AttackSubsystemBuilder optimalSize(Integer val) {
		this.optimalSize = PositiveInteger.of(val);
		return this;
	}

	public AttackSubsystemBuilder optimalSpeed(Integer val) {
		this.optimalSpeed = PositiveInteger.of(val);
		return this;
	}

	public AttackSubsystemBuilder capacitorUsage(Integer val) {
		this.capacitorUsage = PositiveInteger.of(val);
		return this;
	}

	public AttackSubsystemImpl construct() {
		return AttackSubsystemImpl.construct(this.name, this.pgRequirement, this.capacitorUsage, this.optimalSpeed,
				this.optimalSize, this.baseDamage);
	}

}
