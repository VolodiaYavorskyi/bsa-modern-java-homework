package com.binary_studio.academy_coin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	public static void main(String[] args) {
		Stream<Integer> prices = Stream.of(7, 1, 5, 3, 6, 4);
		System.out.println(maxProfit(prices));
	}

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		return maxProfitHelper(prices.collect(Collectors.toList()));
	}

	private static int maxProfitHelper(List<Integer> prices) {
		int n = prices.size();
		// Zero or one elements
		if (n < 2) {
			return 0;
		}

		int profit = 0;

		// Buy first if next is bigger
		if (prices.get(0) <= prices.get(1)) {
			profit -= prices.get(0);
		}
		for (int i = 1; i < n - 1; i++) {
			// Sell local minimum
			if (prices.get(i - 1) >= prices.get(i) && prices.get(i) <= prices.get(i + 1)) {
				profit -= prices.get(i);
			}
			// Buy local maximum
			else if (prices.get(i - 1) <= prices.get(i) && prices.get(i) >= prices.get(i + 1)) {
				profit += prices.get(i);
			}
		}
		// Buy last if previous is smaller
		if (prices.get(n - 1) >= prices.get(n - 2)) {
			profit += prices.get(n - 1);
		}

		return profit;
	}

}
